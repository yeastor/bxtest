/**
 * Created by admin on 11.03.2017.
 */
(function($){

    Controller = function(){
             this.page = 1;
             this.offset = 5;
             this.url = "/bitrix/components/sb/bxtest.person/ajax.php";
             this.blockId = $('#ib_block_id').val();
             this.personCount = 0;
             this.service = new AjaxService();
             this.view = new View($('.persons-container'));

            this.attachHandlers = function() {
                this.view.events
                    .on('formSubmited', $.proxy(this.onUpdateFormSubmited, this))
                    .on('addFormSubmited', $.proxy(this.onAddFormSubmited, this))
                    .on('pageSelected', $.proxy(this.onPageChanged, this));
            };

        this.onAddFormSubmited = function(e) {
            this.addPerson(e.form);
        };

        this.onUpdateFormSubmited = function (e) {
            this.savePerson(e.form);
        };

        this.savePerson = function (person){
            var promiseSave = this.service.put(this.url,{ blockId: this.blockId, person: person});
            promiseSave.done($.proxy(this.onPersonUpdated,this));
            promiseSave.fail($.proxy(this.failed,this));
        };

        this.addPerson = function (person){
            var promiseAdd = this.service.post(this.url,{ blockId: this.blockId, person: person});
            promiseAdd.done($.proxy(this.onPersonAdded,this));
            promiseAdd.fail($.proxy(this.failed,this));
        };

        this.onPersonAdded = function (res){
            if (this.weNeedAddPageAfterPersonAdded()) {
                this.page = (this.getTotalPages() + 1);
                this.view.addPage(this.page);
                this.getPersons();
            } else if (this.weOnLastPage){
                this.getPersons();
            }

            this.personCount++;
        };
        this.weOnLastPage = function (){
            return (this.getTotalPages() == page);
        };

        this.getTotalPages = function (){
            return (Math.ceil(this.personCount / this.offset));
        };

        this.weNeedAddPageAfterPersonAdded = function () {
            return ((this.personCount % this.offset) == 0);
        };

        this.onPersonUpdated = function (res){
            this.getPersons(); // ����� �������� ������ � �������������
        };

        this.onPageChanged = function (e) {
            this.page = e.pageNumber;
            this.getPersons();
        };

        this.failed = function ( jqXHR, textStatus ){
            alert( "Request failed: " + textStatus );
        };

        this.onGetPersonCount = function (count){
            this.personCount = parseInt(count);
            this.view.createPages(this.personCount,this.offset);
            this.getPersons();
        };

        this.onGetPersonList = function (persons){
            this.view.createPersons(persons);
            this.view.setPage(this.page);
        };

        this.getPersons = function (){
           var personsPromise = this.service.get(this.url,{
                page:this.page,
                offset:this.offset,
                blockId: this.blockId,
                action: 'getPersonList'
            });
            personsPromise.done($.proxy(this.onGetPersonList,this));
            personsPromise.fail($.proxy(this.failed,this));
        };

        this.getPersonCount = function(){
           var countPromise = this.service.get(this.url,{
                page:this.page,
                offset:this.offset,
                blockId: this.blockId,
                action: 'getPersonCount'
            });
            countPromise.done($.proxy(this.onGetPersonCount,this));
            countPromise.fail($.proxy(this.failed,this));
        }

    };

    Controller.prototype = {
        constructor : Controller,
        init : function(){
            this.attachHandlers();
            this.getPersonCount();
        }

    };
    return Controller;
})(jQuery);


