<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.03.2017
 * Time: 12:08
 */
class Response
{
    const TYPE_JSON = "json";
    private $type;
    private $content;

    private function contentToJson() {

        return json_encode($this->content);
        exit;
    }

    private function setHeader() {
        if ($this->type == self::TYPE_JSON) {
            header('Content-Type: application/json');
        }
    }

    public function __construct($content){
        $this->content = $content;
    }



    public function sendData($type) {
        $responseData = "";
        $this->setHeader();
        if ($type == self::TYPE_JSON) {
            $responseData = $this->contentToJson();
        }
        echo  $responseData;
        exit;
    }

}