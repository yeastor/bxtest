<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.03.2017
 * Time: 12:29
 */
class Person implements UserModel
{
    private $id;
    private $fname;
    private $lname;
    private $sname;

    /**
     * ������� ������� �� �������
     * @param array $person
     * @return Person
     */
    public static function createFromArray($person) {
        $instanse = new self;
        foreach ($person as $key => $value){
            if ( property_exists ( $instanse , $key ) ){
                $instanse->{'set'.ucwords($key)}($value);
            }
        }
        return $instanse;

    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * @param mixed $fname
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    }

    /**
     * @return mixed
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * @param mixed $lname
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    }

    /**
     * @return mixed
     */
    public function getSname()
    {
        return $this->sname;
    }

    /**
     * @param mixed $sname
     */
    public function setSname($sname)
    {
        $this->sname = $sname;
    }


}