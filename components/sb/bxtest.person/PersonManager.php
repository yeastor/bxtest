<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.03.2017
 * Time: 13:47
 */
interface PersonManager
{
    /**
     * �������� ������ �������������. ���� � ������.
     * @param $page
     * @param $offset
     * @return array
     */
    public function getList($page, $offset);

    /**
     * ��������� ������������
     * @param UserModel $user
     */
    public function saveUser($user);


    /**
     * �������� ���-�� �������
     * @param mixed $params
     * @return int
     */
    public function getCount($params);

    /**
     * @param mixed $options
     */
    public function setOptions($options);
}