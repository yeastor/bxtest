/**
 * Created by admin on 11.03.2017.
 */
(function($){

    View = function(container){

        this.container = container;
        this.pager = $(this.container).find('.pager');
        this.persons = $(this.container).find('.persons');
        this.currentPage = null;
        this.activeForm = null;
        this.activeButton = null;
        this.events = $({});
        this.createInputFiled = function(type,name,value) {
            return $('<input type="'+type+'" name="'+name+'" value="'+value+'"/>');
        };

        this.initAddForm = function(){
            this.addForm = $(this.container).find('#addForm');
            $(this.addForm).find('#addSubmit').click(this.addButtonClickd().bind(this));
        };

        this.attachHandlers = function(){
            $(this.container).find('.showForm').click(function(e) {
                $(this.addForm).removeClass('hidden')
            }.bind(this));
        };

        this.addButtonClickd = function(e) {
            return function () {
                this.events.trigger($.Event('addFormSubmited',  { form : this.addForm.serialize() }));
            }
        };

        this.onButtonClicked = function(button) {
            return function () {
                var form;
                form = $(button).next('form');

                if (this.activeForm !== null) {
                    this.activeForm.addClass('hidden');
                }
                form.removeClass('hidden');
                this.activeForm = form;
                if (this.activeButton != null && this.activeButton != button) {
                    this.activeButton.val("edit");
                }
                if ($(button).val() == "save"){
                    this.events.trigger($.Event('formSubmited',  { form : this.activeForm.serialize() }));
                } else {
                    $(button).val("save");
                }
                this.activeButton = button;


            };
        };

        this.appendPage = function(i) {
            var pageView;
            pageView = $("<span></span>").text(i);
            pageView.click(this.onPageSelected(i).bind(this));
            this.pager.append(pageView);
        };

        this.initAddForm();
        this.attachHandlers();
    };

    View.prototype = {
        constructor : View,
        createPages: function(recordsCount,offset) {
            var pages = Math.ceil(recordsCount / offset);

            for (i = 1;i<=pages;i++) {
                this.appendPage(i);
            }

        },
        onPageSelected: function(i) {
           return function(){
               this.setPage(i);
               this.events.trigger($.Event('pageSelected', { pageNumber: i}));
           }
        },

        setPage: function(pageNumber){
            var pagesView;
            if (this.currentPage != pageNumber) {
                pagesView = this.pager.find("span");
                $(pagesView[pageNumber - 1]).css( "color", "red" );
                if (this.currentPage !== null)  $(pagesView[this.currentPage - 1]).css( "color", "black" );
                this.currentPage = pageNumber;
            }
        },
        addPage: function(pageNum){
            this.appendPage(pageNum);
        },
        createPersons: function(persons) {
            var personView, personForm, personButton, personContainer;
            personForm;
            $(this.persons).empty();
            $.each( persons, function( key, person ) {
                personContainer = $('<div/>');
                personForm = $('<form class="hidden"></form>')
                personForm.append(this.createInputFiled('hidden','id',person[0]));
                personForm.append(this.createInputFiled('text','lname',person[1]));
                personForm.append(this.createInputFiled('text','fname',person[2]));
                personForm.append(this.createInputFiled('text','sname',person[3]));
                personButton = $('<input type="submit" value="edit"/>');
                personButton.click(this.onButtonClicked(personButton).bind(this));
                personView = $('<div class="peson"> '+person[1]+' '+person[2]+' '+person[3]+'</div>');
                personView.append(personButton);
                personView.append(personForm);
                personContainer.append(personView);
                this.persons.append(personContainer);
            }.bind(this));
        }


    };
    return View;
})(jQuery);


