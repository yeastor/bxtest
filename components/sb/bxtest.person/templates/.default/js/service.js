/**
 * Created by admin on 11.03.2017.
 */
(function($){

     AjaxService = function(){

         this.doRequest = function(url,data,method)   // Only visible inside Restaurant()
         {
            return $.ajax({
                 url: url,
                 method: method,
                 type: method,
                 data: data,
                 dataType: "json"
             });
         }

    };

    AjaxService.prototype = {
        constructor : AjaxService,
        get: function(url,data) {
            return this.doRequest(url,data,"GET");
        },
        post: function(url,data) {
            return this.doRequest(url,data,"POST");
        },
        put: function(url,data) {
            return this.doRequest(url,data,"POST");
        },
    };
    return AjaxService;
})(jQuery);


