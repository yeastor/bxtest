<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.03.2017
 * Time: 13:42
 */
interface UserModel
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getFname();

    /**
     * @param string $fname
     */
    public function setFname($fname);

    /**
     * @return string
     */
    public function getLname();

    /**
     * @param string $lname
     */
    public function setLname($lname);

    /**
     * @return string
     */
    public function getSname();

    /**
     * @param string $sname
     */
    public function setSname($sname);

}