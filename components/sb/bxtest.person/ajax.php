<?php
ini_set("display_errros",1);
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
require_once("UserModel.php");
require_once("PersonManager.php");
require_once("class.php");
require_once("Person.php");
require_once("Response.php");
if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $blockId = $_GET['blockId'];

    $pm = new BxPersonManager();
    $pm->setOptions($blockId);

    if ($_GET['action'] === "getPersonCount"){
        $count = $pm->getCount($blockId);
        sendJsonResponse($count);
    }

    if ($_GET['action'] === 'getPersonList') {
        $page = $_GET['page'];
        $offset = $_GET['offset'];
        $blockId = $_GET['blockId'];
        $users = $pm->getList($page,$offset);
        sendJsonResponse($users);
    }
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $blockId = $_POST['blockId'];
    $pm = new BxPersonManager();
    $pm->setOptions($blockId);
    $personArr = queryStringToArray($_POST['person']);
    $person = Person::createFromArray($personArr);

    $pm->saveUser($person);
    sendJsonResponse($person);
}

/**
 * @param $data
 */
function sendJsonResponse($data) {
    $response =  new Response($data);
    $response->sendData(Response::TYPE_JSON);
}

/**
 * @param $string
 * @return array
 */
function queryStringToArray($string) {
    parse_str($string, $array);
    return $array;
}