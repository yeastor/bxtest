<?php
class BxPersonManager extends CBitrixComponent //implements PersonManager
{

    private $pblockId;


    /**
     * @param $blockId
     */
    public function  setOptions($blockId)
    {
        $this->pblockId = $blockId;
    }

    /**
     * �������� ������������� �� ��������
     * @param $page
     * @param $offset
     * @return array
     */
    public function getList($page, $offset)
    {
        $arSelect = Array("ID", "NAME", "fname", "PROPERTY_LNAME", "PROPERTY_FNAME", "PROPERTY_SNAME");
        $arFilter = Array("IBLOCK_ID" => IntVal($this->pblockId), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => $offset, "iNumPage" => $page), $arSelect);
        $users = array();
        while ($ob = $res->GetNextElement()) {

            $arFields = $ob->GetFields();

            $users[] = array(
                $arFields["ID"],
                iconv("cp1251", "utf-8", $arFields['PROPERTY_LNAME_VALUE']),
                iconv("cp1251", "utf-8", $arFields['PROPERTY_FNAME_VALUE']),
                iconv("cp1251", "utf-8", $arFields['PROPERTY_SNAME_VALUE'])

            );


        }

        return $users;

    }

    /**
     * @param UserModel $user
     */
    public function saveUser($user)
    {

        if (!$user->getId()) {
            $user = $this->addUser($user);
        };
        //TODO: ������� ��������� �����
        CIBlockElement::SetPropertyValues($user->getId(), $this->pblockId, iconv("utf-8", "cp1251", $user->getLname()), "LNAME");
        CIBlockElement::SetPropertyValues($user->getId(), $this->pblockId, iconv("utf-8", "cp1251", $user->getFname()), "FNAME");
        CIBlockElement::SetPropertyValues($user->getId(), $this->pblockId, iconv("utf-8", "cp1251", $user->getSname()), "SNAME");

    }

    /**
     * @param UserModel $user
     * @return UserModel
     */
    private function addUser($user){
        $el = new CIBlockElement;
        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,          // ������� ����� � ����� �������
            "IBLOCK_ID" => $this->pblockId,
            "NAME" => "�������",
            "ACTIVE" => "Y"
        );
        $addedID = $el->Add($arLoadProductArray);
        $user->setId($addedID);
        return $user;
    }

    /**
     * �������� ���-�� ������� � ���������
     * @param int $IB_id
     * @return int
     */
    public function getCount($IB_id)
    {
        $cnt = CIBlockElement::GetList(
            array("ID" => "ASC"),
            array('IBLOCK_ID' => $IB_id),
            array(),
            false,
            array('ID', 'NAME')
        );
        return $cnt;
    }
}